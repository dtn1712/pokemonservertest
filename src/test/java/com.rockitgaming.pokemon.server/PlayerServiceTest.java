package com.rockitgaming.pokemon.server;

import com.rockitgaming.pokemon.data.model.TableName;
import com.rockitgaming.pokemon.data.model.entity.account.Account;
import com.rockitgaming.pokemon.data.model.entity.item.Item;
import com.rockitgaming.pokemon.data.model.entity.player.*;
import com.rockitgaming.pokemon.data.model.service.AccountService;
import com.rockitgaming.pokemon.data.model.service.PlayerService;
import com.zaxxer.hikari.HikariDataSource;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "/spring/beanconfig/application-context.xml")
public class PlayerServiceTest {

    @Autowired
    private PlayerService playerService;

    @Autowired
    private AccountService accountService;

    @Autowired
    private HikariDataSource dataSource;

    private JdbcTemplate jdbcTemplate;

    private Account account;

    @Before
    public void setup() {
        jdbcTemplate = new JdbcTemplate(dataSource);
        clearDatabase();

        setupItem();
        setupAccount();
        playerService.createPlayer(generatePlayer(account));
    }

    @After
    public void tearDown() {
        clearDatabase();
        jdbcTemplate = null;
    }

    @Test
    public void testGetPlayer() {
        Player player = playerService.getPlayer("test");
        Assert.assertTrue(playerService.existsByName("test"));
        Assert.assertFalse(playerService.existsByName("test2"));
        Assert.assertNotNull(player.getGender());
        Assert.assertNotNull(player.getPosX());
    }

    private Player generatePlayer(Account account) {
        Item clothing = new Item();
        Item hat = new Item();
        clothing.setId(1L);
        hat.setId(2L);

        Player player = new Player();
        player.setName("test");
        player.setAccount(account);
        player.setGender(1);
        player.setHairColor(1);
        player.setHairStyle(1);
        player.setMapX(0);
        player.setMapY(0);
        player.setLanguage(LanguageEnum.VIETNAMESE);
        player.setPosX(0);
        player.setPosY(0);
        player.setMuted(false);
        player.setSpeed(5);
        player.setSkin(0);
        player.setSprite(0);
        player.setClothing(clothing);
        player.setHat(hat);
        player.setMoney(0L);
        player.setPlayerStatus(PlayerStatusEnum.ACTIVE);
        player.setPlayerType(PlayerTypeEnum.NORMAL);
        return player;
    }

    private void setupItem() {
        jdbcTemplate.execute("INSERT INTO itempocket (itempocket.id, itempocket.name, itempocket.key) VALUES (1, 'pocket1Name', 'pocket1Key')");
        jdbcTemplate.execute("INSERT INTO itemcategory (itemcategory.id, itemcategory.name, itemcategory.key, itemcategory.pocketId) VALUES (1, 'category1Name', 'category1Key', 1)");
        jdbcTemplate.execute("INSERT INTO item (item.id,item.name,item.cost,item.effectDescription,item.categoryId) VALUES (1,'clothing1',100,'clothing 1 description', 1)");
        jdbcTemplate.execute("INSERT INTO item (item.id,item.name,item.cost,item.effectDescription,item.categoryId) VALUES (2,'hat1',50,'hat 1 description', 1)");
    }

    private void setupAccount() {
        account = new Account();
        account.setEmail("test1@gmail.com");
        account.setUsername("test1");
        account.setPassword("123456");
        accountService.register(account);
    }

    private void clearDatabase() {
        jdbcTemplate.execute("DELETE FROM " + TableName.PLAYER_TABLE);
        jdbcTemplate.execute("DELETE FROM " + TableName.PLAYER_BAG_ITEM_TABLE);
        jdbcTemplate.execute("DELETE FROM " + TableName.PLAYER_PARTY_TABLE);
        jdbcTemplate.execute("DELETE FROM " + TableName.PLAYER_POKEDEX_TABLE);
        jdbcTemplate.execute("DELETE FROM " + TableName.ACCOUNT_LOCK_TABLE);
        jdbcTemplate.execute("DELETE FROM " + TableName.ACCOUNT_ATTEMPT_LOGIN_TABLE);
        jdbcTemplate.execute("DELETE FROM " + TableName.ACCOUNT_TABLE);
        jdbcTemplate.execute("DELETE FROM " + TableName.ITEM_TABLE);
        jdbcTemplate.execute("DELETE FROM " + TableName.ITEM_CATEGORY_TABLE);
        jdbcTemplate.execute("DELETE FROM " + TableName.ITEM_POCKET_TABLE);
    }
}
