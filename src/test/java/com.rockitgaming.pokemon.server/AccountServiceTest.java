package com.rockitgaming.pokemon.server;

import com.rockitgaming.pokemon.data.model.TableName;
import com.rockitgaming.pokemon.data.model.entity.account.Account;
import com.rockitgaming.pokemon.data.model.exceptions.AccountAccessException;
import com.rockitgaming.pokemon.data.model.service.AccountService;
import com.zaxxer.hikari.HikariDataSource;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "/spring/beanconfig/application-context.xml")
public class AccountServiceTest {

    @Autowired
    private AccountService accountService;

    @Autowired
    private HikariDataSource dataSource;

    private JdbcTemplate jdbcTemplate;

    @Before
    public void setup() {
        jdbcTemplate = new JdbcTemplate(dataSource);
//        clearDatabase();
    }

    @After
    public void tearDown() {
//        clearDatabase();
        jdbcTemplate = null;
    }

    @Test
    public void testHappyPath() throws Exception{
        Account account = new Account();
        account.setEmail("test1@gmail.com");
        account.setUsername("test1");
        account.setPassword("123456");
        accountService.register(account);
        accountService.login("test1", "123456", "127.0.0.1");
    }

    @Test
    public void testFailedLogin() throws Exception {
        Account account = new Account();
        account.setEmail("test1@gmail.com");
        account.setUsername("test1");
        account.setPassword("123456");
        accountService.register(account);

        for (int i = 0; i < 10; i++) {
            try {
                accountService.login("test1", "1234567890", "127.0.0.1");
            } catch (AccountAccessException e) {}
        }

        Assert.assertTrue(accountService.isAccountLocked("test1", "127.0.0.1"));
    }

    private void clearDatabase() {
        jdbcTemplate.execute("DELETE FROM " + TableName.ACCOUNT_LOCK_TABLE);
        jdbcTemplate.execute("DELETE FROM " + TableName.ACCOUNT_ATTEMPT_LOGIN_TABLE);
        jdbcTemplate.execute("DELETE FROM " + TableName.ACCOUNT_TABLE);
    }
}
