package com.rockitgaming.pokemon.server.network;


public class CommandType {

    public static final short COMMAND_ERROR = -1;
    public static final short COMMAND_SUCCESS = 1;

    public static final short COMMAND_CONNECT = 100;
    public static final short COMMAND_DISCONNECT = 101;
    public static final short COMMAND_REGISTER = 102;
    public static final short COMMAND_LOGIN = 103;
    public static final short COMMAND_LOGOUT = 104;



}
