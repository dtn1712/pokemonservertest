package com.rockitgaming.pokemon.server.network;


public class ErrorCode {

    public static final short ERROR_NO_INPUT= 0;

    public static final short ERROR_INVALID_INPUT = 1;

    public static final short ERROR_ALREADY_LOGIN = 2;
}
