package com.rockitgaming.pokemon.server.network.handler;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.rockitgaming.pokemon.data.model.entity.account.Account;
import com.rockitgaming.pokemon.data.model.service.AccountService;
import com.rockitgaming.pokemon.data.model.util.MapperUtils;
import com.rockitgaming.pokemon.network.domain.Message;
import com.rockitgaming.pokemon.network.domain.Session;
import com.rockitgaming.pokemon.network.socket.handler.BaseRequestHandler;
import com.rockitgaming.pokemon.server.network.CommandType;
import com.rockitgaming.pokemon.server.network.DefaultMessageFactory;
import com.rockitgaming.pokemon.server.network.ErrorCode;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

@Component
public class RegisterRequestHandler extends BaseRequestHandler{

    private static final Logger logger = LogManager.getLogger(RegisterRequestHandler.class);

    private static final String[] REQUIRED_FIELDS = new String[] {"email", "username", "password"};

    @Autowired
    private AccountService accountService;

    @Override
    public void handle(Session session, Message message) {
        ObjectMapper objectMapper = MapperUtils.getObjectMapper();
        String messageStringData = message.getString(Message.DATA_KEY);
        if (messageStringData != null) {
            try {
                Map<String, String> messageData = objectMapper.readValue(messageStringData, Map.class);

                List<String> missingFields = checkMissingFields(REQUIRED_FIELDS, messageData);
                if (missingFields.isEmpty()) {
                    String username = messageData.get("username");
                    String email = messageData.get("email");
                    String password = messageData.get("password");

                    Account account = new Account();
                    account.setUsername(username);
                    account.setEmail(email);
                    account.setPassword(password);

                    Account createdAccount = accountService.register(account);

                    Message successfulMessage = DefaultMessageFactory.createMessage(CommandType.COMMAND_SUCCESS);
                    successfulMessage.putString(Message.DATA_KEY, objectMapper.writeValueAsString(createdAccount));

                    session.setAuthenticated(true);
                    writeMessage(session, successfulMessage);
                } else {
                    writeMessage(session, DefaultMessageFactory.createErrorMessage(ErrorCode.ERROR_INVALID_INPUT,
                            String.format("Missing data input &s", missingFields.toString())));
                }
            } catch (Exception e) {
                logger.error("Failed to register account",e);
                writeMessage(session, DefaultMessageFactory.createErrorMessage(ErrorCode.ERROR_INVALID_INPUT,e.getMessage()));
            }
        } else {
            writeMessage(session, DefaultMessageFactory.createErrorMessage(ErrorCode.ERROR_NO_INPUT,"No input received"));
        }
    }


}
