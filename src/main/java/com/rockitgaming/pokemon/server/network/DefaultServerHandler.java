package com.rockitgaming.pokemon.server.network;

import com.rockitgaming.pokemon.network.domain.Session;
import com.rockitgaming.pokemon.network.socket.SocketServerHandler;
import io.netty.channel.ChannelHandler.Sharable;
import org.springframework.stereotype.Component;

@Component
@Sharable
public class DefaultServerHandler extends SocketServerHandler{

    @Override
    protected void handleConnectedRequest(Session session) {
        /** TODO: Add metric **/
//        messageExecutor.execute(session, DefaultMessageFactory.createConnectMessage());
    }

    @Override
    protected void handleDisconnectedRequest(Session session) {
        /** TODO: Add metric **/
//        messageExecutor.execute(session, DefaultMessageFactory.createDisconnectMessage());
    }
}
