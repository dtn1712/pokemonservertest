package com.rockitgaming.pokemon.server.network;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.rockitgaming.pokemon.data.model.util.MapperUtils;
import com.rockitgaming.pokemon.network.domain.Message;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.HashMap;
import java.util.Map;

public class DefaultMessageFactory {

	private static final Logger logger = LogManager.getLogger(DefaultMessageFactory.class);

	private static final byte PROTOCOL_VERSION = 1;

	public static Message createMessage(short commandId) {
		Message message = new Message();
		message.setCommandId(commandId);
		message.setProtocolVersion(PROTOCOL_VERSION);
		return message;
	}

	public static Message createErrorMessage(short code, String errorMessage) {
		Message message = new Message();
		message.setCommandId(CommandType.COMMAND_ERROR);
		message.setProtocolVersion(PROTOCOL_VERSION);

		Map<String, Object> messageData = new HashMap<>();
		messageData.put("errorCode", code);
		messageData.put("errorMessage", errorMessage);
		try {
			message.putString(Message.DATA_KEY, MapperUtils.getObjectMapper().writeValueAsString(messageData));
		} catch (JsonProcessingException e) {
			logger.error("Failed to convert message data to json string", e);
		}
		return message;
	}


	public static Message createConnectMessage() {
		Message message = new Message();
		message.setCommandId(CommandType.COMMAND_CONNECT);
		message.setProtocolVersion(PROTOCOL_VERSION);
		return message;
	}


	public static Message createDisconnectMessage() {
		Message message = new Message();
		message.setCommandId(CommandType.COMMAND_DISCONNECT);
		message.setProtocolVersion(PROTOCOL_VERSION);
		return message;
	}


}
