package com.rockitgaming.pokemon.server;

import com.rockitgaming.pokemon.network.ServerConfig;
import com.rockitgaming.pokemon.network.socket.SocketServer;
import com.rockitgaming.pokemon.network.socket.channel.ChannelService;
import com.rockitgaming.pokemon.server.util.Utility;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class GameServer {

    public static void initConfig() {
        ServerConfig.init(Utility.loadConfigFile(Constants.CONFIG_FOLDER_PATH + "server.properties"));
    }


    public static void main(String[] args) throws Exception {
        initConfig();

        ApplicationContext context = new ClassPathXmlApplicationContext(Constants.APPLICATION_CONTEXT_PATH);
        SocketServer server = context.getBean(SocketServer.class);
        ChannelService channelService = context.getBean(ChannelService.class);
        System.out.println(channelService);
        server.start();
    }
}
